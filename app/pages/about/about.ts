import {Component} from '@angular/core';
import {NavController, Platform} from 'ionic-angular';
import {PeopleService} from '../../providers/people-service/people-service';
import {Database} from "../../providers/database/database";


@Component({
  templateUrl: 'build/pages/about/about.html',
  providers: [PeopleService]
})
export class AboutPage {

  public logInfo: any;
  public ifLoggedIn: boolean;

  public itemList: Array<Object>;

  constructor(
      private navCtrl: NavController, 
      public peopleService: PeopleService, 
      private platform: Platform, 
      private database: Database ) {

    // this.iflodged();
    this.itemList = [];

    this.login();   
  }

    public onPageDidEnter() {
        this.load();
    }
    
    public load() {
        this.database.getPeople().then((result) => {
            this.itemList = <Array<Object>> result;
        }, (error) => {
            console.log("ERROR: ", error);
        });
    }
 
    public create(firstname: string, lastname: string, token: string) {
        this.database.createPerson(firstname, lastname, token).then((result) => {
            this.load();
        }, (error) => {
            console.log("ERROR: ", error);
        });
    }

    public remove() {
        this.database.removeToken().then((result) => {
            this.load();
        }, (error) => {
            console.log("ERROR: ", error);
        });
    }

  login(){
    this.peopleService.load()
    .then(data => {
      this.logInfo = data;
    });
  }


  // iflodged(){
  //   this.ifLoggedIn = false;
  // }
}
