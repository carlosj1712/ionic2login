import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {SQLite} from 'ionic-native';
import 'rxjs/add/operator/map';

/*
  Generated class for the Database provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Database {

    private storage: SQLite;
    private isOpen: boolean;
 
    public constructor(private http: Http) {
        if(!this.isOpen) {
            this.storage = new SQLite();
            this.storage.openDatabase({name: "datatest.db", location: "default"}).then(() => {
                this.storage.executeSql("CREATE TABLE IF NOT EXISTS mydb (id INTEGER PRIMARY KEY AUTOINCREMENT, firstname TEXT, lastname TEXT, token TEXT)", []);
                this.isOpen = true;
            });
        }
    }

    public getPeople() {
        return new Promise((resolve, reject) => {
            this.storage.executeSql("SELECT * FROM mydb", []).then((data) => {
                let people = [];
                if(data.rows.length > 0) {
                    for(let i = 0; i < data.rows.length; i++) {
                        people.push({
                            id: data.rows.item(i).id,
                            firstname: data.rows.item(i).firstname,
                            lastname: data.rows.item(i).lastname,
                            token: data.rows.item(i).token
                        });
                    }
                }
                resolve(people);
            }, (error) => {
                reject(error);
            });
        });
    }
 
    public createPerson(firstname: string, lastname: string, token: string) {
        return new Promise((resolve, reject) => {
            this.storage.executeSql("INSERT INTO mydb (firstname, lastname, token) VALUES (?, ?, ?)", [firstname, lastname, token]).then((data) => {
                resolve(data);
            }, (error) => {
                reject(error);
            });
        });
    }

    // Remoe a not with a given ID \"' + note.id + '\"
    public removeToken() {
        return new Promise((resolve, reject) => {
            this.storage.executeSql("DELETE FROM mydb WHERE 1 ", []).then((data) => {
                resolve(data);
            }, (error) => {
                reject(error);
            });
        });        
    }

}

